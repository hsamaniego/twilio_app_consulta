@extends('layouts.app')

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style>
    #izquierda{
        width: 30% !important;
    }

    #derecha{
        width: 95% !important;
    }

</style>
<script type="text/javascript">
$(document).ready(function(){

    $('#phone_number').on('input', function () { 
        this.value = this.value.replace(/[^+0-9]/g,'');
    });
});
</script>

@section('content')
<div class="container">
    <div class="container">
        <div class="jumbotron">
            <!--<div class="row">-->
                <div  id="izquierda">
                    @if (Session::has('success1'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('success1')}}
                        </div>
                    @endif

                    @if (Session::has('error'))
                        <div class="alert alert-danger" role="alert">
                            {{Session::get('error')}}
                        </div>
                    @endif

                    @if (@$warning)
                        <div class="alert alert-warning" role="alert">
                            {!!$warning!!}
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header">
                            Recuperar mensajes
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('list') }}" >
                                @csrf
                                <div class="form-group">
                                    <label>Ingrese n&uacute;mero:</label>
                                    <input required id="phone_number" placeholder="+595981321654" name="phone_number" type="text" >
                                </div>
                                <button type="submit" class="btn btn-primary">Verificar</button>
                            </form>
                        </div>
                    </div>
                </div>
                <p>
                <div id="derecha">
                    @if (Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            Mensajes enviados
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>Texto</th>
                                    <th>N&uacute;mero env&iacute;o</th>
                                    <th>N&uacute;mero destino</th>
                                    <th>Estado</th>
                                    <th>Fecha env&iacute;o</th>
                                    <th>Fecha creaci&oacute;n</th>
                                    <th>Fecha actualizaci&oacute;n</th>
                                    <th>Mensaje de error</th>
                                    <th>C&oacute;digo de error</th>
                                  </tr>
                                </thead>
                                
                                <tbody>
                                    @foreach($messages as $mjs)
                                    
                                    @php
                                        
                                        $fecha = date_format($mjs->dateSent,'Y-m-d H:i:s');
                                        $fechaC = date_format($mjs->dateCreated,'Y-m-d H:i:s');
                                        $fechaU = date_format($mjs->dateUpdated,'Y-m-d H:i:s');
                                    @endphp
                                     
                                    <tr>
                                        <td>{!!$mjs->body!!}</td>
                                        <td>{!!$mjs->from!!}</td>
                                        <td>{!!$mjs->to!!}</td>
                                        <td>
                                            @if($mjs->status == "failed" || $mjs->status == "undelivered")
                                                <label style="color:red; font-weight: bold;">{!!$mjs->status!!}</label>
                                            @else
                                            <label style="color:green; font-weight: bold;">{!!$mjs->status!!}</label>
                                            @endif
                                        </td>
                                        <td>{!!$fecha!!}</td>
                                        <td>{!!$fechaC!!}</td>
                                        <td>{!!$fechaU!!}</td>
                                        <td>{!!$mjs->errorMessage!!}</td>
                                        <td>{!!$mjs->errorCode!!}</td>
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                                
                              </table>
                        </div>
                    </div>
                </div>
            <!--</div>-->
        </div>
    </div>
</div>
@endsection

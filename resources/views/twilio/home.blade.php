@extends('layouts.app')

@section('content')
<div class="container">
    <div class="container">
        <div class="jumbotron">
            <div class="row">
                <div class="col">
                    @if (Session::has('success'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            Agregar n&uacute;mero de t&eacute;lefono
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('store') }}" >
                                @csrf
                                <div class="form-group">
                                    <label>Ingrese n&uacute;mero</label>
                                    <input type="tel" name="phone_number" class="form-control" placeholder="Ingrese n&uacute;mero">
                                </div>
                                <button type="submit" class="btn btn-primary">Registrar N&uacute;mero</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col">
                    @if (Session::has('success1'))
                        <div class="alert alert-success" role="alert">
                            {{Session::get('success1')}}
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            Enviar Mensaje
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('custom') }}" >
                                @csrf
                                <div class="form-group">
                                    <label>Ingrese n&uacute;mero:</label>
                                    <input id="phone_number" name="phone_number" type="text" >
                                </div>
                                <div class="form-group">
                                    <label>Mensaje</label>
                                 <textarea name="text" class="form-control" rows="3"></textarea>
                                </div>
                                    <button type="submit" class="btn btn-primary">Enviar Notificaci&oacute;n</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

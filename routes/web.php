<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/twilio', [App\Http\Controllers\TwilioController::class, 'index'])->name('twilio');
Route::get('/twilioview', [App\Http\Controllers\TwilioController::class, 'view'])->name('twilioview');
Route::post('/store', [App\Http\Controllers\TwilioController::class, 'storePhoneNumber'])->name('store');
Route::post('/custom', [App\Http\Controllers\TwilioController::class, 'sendCustomMessage'])->name('custom');
Route::post('/list', [App\Http\Controllers\TwilioController::class, 'listSent'])->name('list');

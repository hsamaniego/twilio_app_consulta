

## Aplicación para consulta de envío sms - DOCKER - LARAVEL - PHP - MYSQL - NGINX

Consulta de estado de envíos de sms
## :: SE DEBE TENER INSTALADO DOCKER ::
## Manual de uso

- Clonar repositorio
````sh
git clone [reposirotio-url] [nombre]
````
- Ubicarse dentro del proyecto [nombre]
````sh
cd [nombre]
````
- Editar la variable de conexión a BD en el archivo .env en la raíz del proyecto
````sh
DB_HOST=[ip_maquina_local]
````
- Claves Twilio editar en app/Http/Controllers/TwilioController.php
````sh
linea: 106,107
````
- Ejecutar comando docker-compose
````sh
docker-compose up -d
````
- Ejecutar docker ps para verificar el container id del laravel-app
````sh
docker ps
````
- Acceder al container laravel-app con el siguiente comando
````sh
docker exec -it [container_id] /bin/bash
````
- Ejecutar los siguientes comandos artisan dentro del contenedor migrate para migrar las tablas y optimize para limpiar y recargar configuraciones de aplicación
````sh
php artisan migrate
php artisan optimize
````
- Acceder desde navegador
  ````sh
  http://127.0.0.1/
  ````
  
 - **Bugs y Consultas: higinio.samaniego@itti.digital**
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsersPhoneNumber;
use Twilio\Rest\Client;

class TwilioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('twilio.home');
    }

    /**
     * Store a new user phone number.
     *
     * @param  Request  $request
     * @return Response
     */
    public function storePhoneNumber(Request $request)
    {
       
        //run validation on data sent in
        /*$validatedData = $request->validate([
            'phone_number' => 'required|unique:users_phone_number|max:13'
        ]);*/
        $user_phone_number_model = new UsersPhoneNumber($request->all());
        $user_phone_number_model->save();
        $this->sendMessage('User registration successful!!', $request->phone_number);
        return back()->with(['success'=>"{$request->phone_number} registrado"]);
    }


    /**
     * Sends sms to user using Twilio's programmable sms client
     * @param String $message Body of sms
     * @param Number $recipients string or array of phone number of recepient
     */
    private function sendMessage($message, $recipients)
    {
       

        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");

       

        $client = new Client($account_sid, $auth_token);
        $client->messages->create($recipients, 
                ['from' => $twilio_number, 'body' => $message] );
    }



    /**
     * Send message to a selected users
     */
    public function sendCustomMessage(Request $request)
    {
        /*$validatedData = $request->validate([
            'users' => 'required|array',
            'body' => 'required',
        ]);*/
        $recipient = $request->phone_number;
        $body = $request->text;
        
        // iterate over the array of recipients and send a twilio request for each
        //foreach ($recipients as $recipient) {
            $this->sendMessage($body, $recipient);
        //}
        return back()->with(['success1' => "Mensaje en camino!"]);
    }

    /**
     * Send message to a selected users
     */
    public function view()
    {
        return view('twilio.homeview');
    }

    /****
     * Response for items to sent
     */
    public function listSent(Request $request){
        // Your Account Sid and Auth Token from twilio.com/user/account
        
        $sid = "ACd1ffdba7659feb341ac1f19b72498e28";
        $token = "6e2f8884dcbaf442cef3272c36c5bcab";

        $twilio = new Client($sid, $token);
       
        if(isset($request->phone_number)){


            $messages = $twilio->messages
            ->read(["to" => $request->phone_number], 1);
        
            if(count($messages) > 0){
                return view('twilio.homeview2',compact('messages'));
            }else{
            
                $warning = "No existen registros para el número: $request->phone_number";
                return view('twilio.homeview',compact('warning'));
            }
            
        }else{
            $error =  "Ingrese número de celular";
            return view('twilio.homeview',compact('error'));
            //return back()->with(['error' => "Ingrese número de celular"]);
        }
        
    }


}
